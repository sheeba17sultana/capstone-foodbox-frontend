import { environment } from "src/environments/environment";

export const baseUrl = environment.production ? 'https://foodbox.cfapps.io/' : 'http://localhost:7443'
export const restaurantUrl = baseUrl + '/restaurant'
export const foodUrl = baseUrl + '/food'
export const userUrl = baseUrl + '/user'
export const cartUrl = baseUrl + '/cart'
export const orderUrl = baseUrl + '/order'